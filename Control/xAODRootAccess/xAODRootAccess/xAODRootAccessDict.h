// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: xAODRootAccessDict.h 796516 2017-02-10 04:45:05Z ssnyder $
#ifndef XAODROOTACCESS_XAODROOTACCESSDICT_H
#define XAODROOTACCESS_XAODROOTACCESSDICT_H

// Local includude(s):
#include "xAODRootAccess/MakeTransientTree.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TPyEvent.h"
#include "xAODRootAccess/TPyStore.h"
#include "xAODRootAccess/TTreeMgr.h"
#include "xAODRootAccess/tools/TVirtualManager.h"

#endif // XAODROOTACCESS_XAODROOTACCESSDICT_H
